const { Schema, model } = require("mongoose");

const ClotheSchema = Schema(
  {
    tipo: {
      type: String,
      required: true,
      enum: ["buzo", "remera", "campera", "pantalón"],
    },
    cantidad: {
      type: Number,
      required: true,
    },
    precio: {
      type: Number,
      required: true,
    },
    descripcion: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("Clothes", ClotheSchema);
